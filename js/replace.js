// vim:set sw=2 ts=2 sts=2 expandtab:
// List of strings to proutify
const prouts = {
  "darmanin": "darmaprout",
  "Darmanin": "Darmaprout",
  "DARMANIN": "DARMAPROUT",
  "Bruno Le Maire": "Bruno Le Prout",
  "Bruno Lemaire": "Bruno Le Prout",
  "Jean Castex": "Jean Proutex",
  "Macron": "Maprout",
  "Barbara Pompili": "Barbara Proutili",
  "Jean-Michel Blanquer": "Jean-Michel Blanc-Prout",
  "Marlène Schiappa": "Marlène Schiaprout",
  "Élisabeth Borne": "Élisabeth Prout",
  "Elisabeth Borne": "Élisabeth Prout",
  "Éric Dupond-Moretti": "Éric Duprout-Moretti",
  "Gabriel Attal": "Gabriel Prouttal",
  "Jean-Yves Le Drian": "Jean-Prout Le Drian",
  "Jean Yves Le Drian": "Jean-Prout Le Drian",
  "Jean-Yves le Drian": "Jean-Prout Le Drian",
  "Agnès Pannier-Runacher": "Agnès Proutier-Runacher",
  "Cédric O": "Cédric prOut",
  "Cedric O": "Cédric prOut",
  "Florence Parly": "Florence Proutly",
  "Roselyne Bachelot": "Proutelyne Bachelot",
  "Olivier Véran": "Proutivier Véran",
  "d'Olivier Véran": "de Proutivier Véran",
  "Frédérique Vidal": "Frédérique Proutal",
  "Frederique Vidal": "Frédérique Proutal",
  "Amélie de Montchalin": "Amélie de Proutalin",
  "Amélie De Montchalin": "Amélie de Proutalin",
  "président": "proutident",
  "Président": "Proutident",
  "Franck Riester": "Franck Prouster",
  "Emmanuelle Wargon": "Emmanuelle Prouton",
  "Jean-Baptiste Djebbari": "Jean-Baptiste Djeprouti",
  "Olivier Dussopt": "Olivier Duprout",
  "Geneviève Darrieussecq": "Geneviève Duproutsecq",
  "Sébastion Lecornu": "Sébastien Leproutu",
  "\Wministre": "miniprout",
  "Ministre": "Miniprout",
  "député": "déprouté",
  "Député": "Déprouté",
  "sénateur": "sénaprout",
  "Sénateur": "Sénaprout",
  "plan de relance": "prout de relance",
  "ce que l'on sait": "ce que l'on proute",
  "couac": "prout",
  "Trump": "Trumpet",
  "Nicolas Sarkozy": "Nicoprout Sarkozy",
  "Geoffroy Roux de Bézieux": "Geoffroy Prout de Bézieux",
  "Le Pen": "Le Prout",
  "Christian Estrosi": "Christian Estroprout",
  "Éric Ciotti": "Éric Proutti",
  "Eric Ciotti": "Éric Proutti",  
  "Mélenchon": "Mélenprout",
  "Castaner": "Castaprout",
  "Zemmour": "Zeprout",
  "Boris": "Beauprout",
  "Covid":"Coprout",
  "Coronavirus":"Coronaproutus",
  "confinement":"confiprout",
  "Poutine":"Proutine",
  "Sergueï Lavrov":"Sergueï Lavprout",
  "Gazprom":"Gazprout",
  "Erdoğan":"Erdoprout",
  "Erdogan":"Erdoprout",
  "Bashar al-Assad":"Bashar al-Asprout",
  "Jair Bolsonaro": "Jair Bolsonaprout",
  "Florian Philippot": "Floriant Philiprout",
  "Jordan Bardella": "Jordan Proutella",
};

// Join prouts in a regex
let regexString = "";
for (let key in prouts) {
  regexString += key + '|';
}
regexString = regexString.slice(0, -1); // Remove trailing pipe

const regex = new RegExp(regexString, "g");
const replacer = name => {
  return prouts[name];
};
var textNode;

// To deal with webpage dynamically loaded, rerun proutify every half a second
setInterval(() => {
  let walk = document.createTreeWalker(document.body, NodeFilter.SHOW_TEXT, null, false);
  
  // Replace in title
  document.title = document.title.replace(regex, replacer);
  
  // Replace in body
  while (textNode = walk.nextNode()) {
    textNode.nodeValue = textNode.nodeValue.replace(regex, replacer);
  }

}, 500)
